function validateForm(el) {
	
	// Get the form element
	var formElement = document.querySelector(el);

	// Find all the form elements inside it
	var inputs = formElement.querySelectorAll('input,select,textarea');

	console.log(el, formElement, inputs.length);


	formElement.addEventListener('submit', function(evt) {
		var thereWasAnError = false;

		for(var i = 0; i < inputs.length; i += 1) {
			var fieldIsValid = processField(inputs[i]);
			if ( ! fieldIsValid) {
				thereWasAnError = true;
			}
		}

		if (thereWasAnError) {
			evt.preventDefault();
		}
	});

	for (var i = 0; i < inputs.length; i += 1) {
		var formInput = inputs[i];
		formInput.addEventListener("blur", function(evt) {
			processField(evt.target);
		});
	}

	function processField(field) {

		console.log(field.id, field.classList);
		if ( ! field.id) return true;

		var fieldMessage = document.querySelector('#' + field.id + "-message");
		if ( ! fieldMessage) {
			throw new Error("Couldn't find validation error message element [#" + field.id + "-message]");
		}

		var fieldIsValid = true;
		fieldMessage.innerHTML = "";

		console.log("CLASSES:", field.className.split(" "));

		var classes = field.className.split(" ");

		classes.forEach(chooseClassValidation);

		return fieldIsValid;

		// Targeting the classes on the inputs,selects,textareas eg. v-required
		function chooseClassValidation(theClass) {
			var thisValidationIsValid = true;
			var classFragments = theClass.split("-");
			console.log(classFragments);

			if (classFragments[0] !== "v") return;

			var validationMethod = classFragments[1];

			if (validationMethod == 'required') {
				thisValidationIsValid = validate_required(field, fieldMessage);
			}

			if (validationMethod == 'min') {
				thisValidationIsValid = validate_min(field, fieldMessage, classFragments[2]);
			}

			if (validationMethod == 'max') {
				thisValidationIsValid = validate_max(field, fieldMessage, classFragments[2]);
			}

			if (validationMethod == 'email') {
				thisValidationIsValid = validate_email(field, fieldMessage);
			}

			if (validationMethod == 'not') {
				thisValidationIsValid = validate_not_negative(field, fieldMessage);
			}

			if (validationMethod == 'no') {
				thisValidationIsValid = validate_no_html(field, fieldMessage);
			}

			if (validationMethod == 'date') {
				thisValidationIsValid = validate_date(field, fieldMessage);
			}

			if (! thisValidationIsValid) {
				fieldIsValid = false;
			}
		}
	}

	// Creating unique span messages
	function validate_required(field, fieldMessage) {
		if (field.type === "checkbox") {
			if ( ! field.checked) {
				fieldMessage.innerHTML = "* Must be checked";
				return false;
			}
		} else if (field.value.trim() == "") {
			fieldMessage.innerHTML = "* Required";
			return false;
		}
		return true;
	}

	function validate_min(field, fieldMessage, minLength) {
		if (field.value.trim().length < minLength) {
			fieldMessage.innerHTML = "* Must be at least " + minLength + " characters long";
			return false;
		}
		return true;
	}

	function validate_max(field, fieldMessage, maxLength) {
		if (field.value.trim().length > maxLength) {
			fieldMessage.innerHTML = "* Must be less than " + maxLength + " characters long";
			return false;
		}
		return true;
	}

	function validate_email(field, fieldMessage) {
		var emailAddressPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
		if ( ! emailAddressPattern.test(field.value.trim())) {
			fieldMessage.innerHTML = "* Please enter a valid email";
			return false;
		}
		return true;
	}

	function validate_not_negative(field, fieldMessage) {
		var valueNumeric = parseFloat(field.value);
		if (valueNumeric < 0) {
			if (field.tagName.toLowerCase() === "select") {
				fieldMessage.innerHTML = "* Select an option";
			} else {
				fieldMessage.innerHTML = "* Must not be negative";
			}
			return false;
		}
		return true;
	}

	function validate_no_html(field, fieldMessage) {
		var htmlPattern = /\<\S|\S\>/;
		// no < or > 

		if (htmlPattern.test(field.value.trim())) {
			fieldMessage.innerHTML = "* No HTML tags allowed";
			return false;
		}
		return true;
	}

	function validate_date(field, fieldMessage) {
		var datePattern = /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;

		if (datePattern.test(field.value.trim())) {
			fieldMessage.innerHTML = "* Insert a valid date";
			return false;
		}
		return true;
	}
}
